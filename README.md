# Bochs - For Redox OS

This the bochs repo, modified to run on Redox OS. The steps for compilation and execution on Redox OS are given below. Note that there are many unresolved bugs and a few disabled features, and it is unstable.

modifications author: tarun.aditya@hotmail.com (mattermost chat username: enygmator)


## Compilation and Execution

> This repo is meant to be compiled from and run on Redox OS, and while it can be run on other platforms, the responsibility of `configure`-ing it correctly, lies on you, in order to compile it successfully. You can refer the bochs developer documentation for more details on how to do that.  
  

1. Assume you have Redox OS in the path `$TopLevel/redox`, where `$TopLevel` can be any path.
2. Place this repo in `$TopLevel/redox_apps/`, such that the path of this `README.md` file is `$TopLevel/redox_apps/bochs/README.md`.
   > If you want to change this location, the instructions to do that are given at the end of this section of the README. 
3. Go to `$TopLevel/redox` in the terminal and run the command `make cook PKG=bochs`.
   NOTE: Before running the command, it would be better if you have compiled and run Redox OS at least once (since, `prefix` is required to have been created correctly for this command to work).
4. If the command runs successfully, then you can proceed to include the "cooked" package, in the final `harddrive.bin` that emulated the harddrive of Redox OS, when run in QEMU.
   To do that, you'll simply have to include the line `bochs = {}` in `filesystem.toml`, under the "table" `[packages]`.
   > `filesystem.toml` points to one of the files in the `config/` folder.
5. Now, it's time to compile and generate `harddrive.bin` with Redox OS and bochs installed on it, and load it into QEMU.
   To do that, go to `$TopLevel/redox` in the terminal and execute:
   ```
   touch filesystem.toml
   make qemu
   ```
6. Inside `Redox OS`, you can open the terminal and run the command `bochs`, which will open up the bochs CLI. You can use "regular" bochs commands, including an emulation involving GUI using the SDL2 GUI Library.

The "recipe" to "cook" this program/`package` can be found in the Redox OS "cookbook", under the folder `bochs` (`$TopLevel/redox/cookbook/recipes/bochs/`). The recipe has all the commands to compile and install bochs in `harddrive.bin`, and assumes the location of `bochs`, which you can change, by changing the variable `BOCHS_PATH` in `$TopLevel/redox/cookbook/recipes/bochs/recipe.sh`

For more info, read the `README.md` in `$TopLevel/redox/cookbook/recipes/bochs/` to understand how to customize the compilation and installation.

### Bochs running a mini bootloader on Redox

![Bochs on Redox Demo](bochs_redox_demo_1.jpg)

-----

## WHAT IS BOCHS?

Bochs is a highly portable open source IA-32 (x86) PC emulator
written in C++, that runs on most popular platforms.  It includes
emulation of the Intel x86 CPU, common I/O devices, and a custom
BIOS. Bochs can be compiled to emulate many different x86 CPUs,
from early 386 to the most recent x86-64 Intel and AMD processors
which may even not reached the market yet. Bochs is capable of running
most Operating Systems inside the emulation, for example DOS,
Linux or Windows. Bochs was written by Kevin Lawton and is currently
maintained by the Bochs project at "https://bochs.sourceforge.io".

Bochs can be compiled and used in a variety of modes, some which are
still in development.  The 'typical' use of bochs is to provide
complete x86 PC emulation, including the x86 processor, hardware
devices, and memory.  This allows you to run OS's and software within
the emulator on your workstation, much like you have a machine
inside of a machine.  Bochs will allow you to run Windows
applications on a Solaris machine with X11, for example.

Bochs is distributed under the GNU LGPL. See LICENSE and COPYING for details.

## GETTING CURRENT SOURCE CODE

Source code for Bochs is available from the Bochs home page at
https://bochs.sourceforge.io.  You can download the most recent
release, use SVN to get the latest sources, or grab a SVN
snapshot which is updated frequently. The releases contain the most
stable code, but if you want the very newest features try the
SVN version instead.

## WHERE ARE THE DOCS?

The Bochs documentation is written in Docbook.  Docbook is a text
format that can be rendered to many popular browser formats such
as HTML, PDF, and Postscript.  Each binary release contains the
HTML rendering of the documentation.  Also, you can view the
latest documentation on the web at
  https://bochs.sourceforge.io/doc/docbook/index.html

## WHERE CAN I GET MORE INFORMATION?  HOW DO I REPORT PROBLEMS?

Both the documentation and the Bochs website have instructions on how
to join the bochs-developers mailing list, which is the primary
forum for discussion of Bochs.  The main page of the website also
has links to bug reports and feature requests.  You can browse and
add to the content in these areas even if you do not have a (free)
SourceForge account.  We need your feedback so that we know what
parts of Bochs to improve.

There is a patches section on the web site too, if you have made
some changes to Bochs that you want to share.

## HOW CAN I HELP?

If you would like contribute to the Bochs project, a good first step
is to join the bochs-developers mailing list, and read the archive
of recent messages to see what's going on.

If you are a technical person (can follow hardware specs, can write
C/C++) take a look at the list of open bug reports and feature
requests to see if you are interested in working on any of the
problems that are mentioned in them.  If you check out the SVN
sources, make some changes, and create a patch, one of the
developers will be very happy to apply it for you.  Developers who
frequently submit patches, or who embark on major changes in the
source can get write access to SVN.  Be sure to communicate with the
bochs-developers list to avoid several people working on the same
thing without realizing it.

If you are a Bochs user, not a hardware/C++ guru, there are still
many ways you could help out.  For example:
  - write instructions on how to install a particular operating system
  - writing/cleaning up documentation
  - testing out Bochs on every imaginable operating system and
    reporting how it goes.

## CHANGES

Brief summary of changes in 2.7:
  - Bugfixes for CPU emulation correctness (CPUID/VMX/SVM fixes to support
    Windows Hyper-V as guest in Bochs)
  - Improvements for the plugin handling to simplify Bochs extensions
  - Added "multiple NICs" support to the NE2000 and E1000 devices
  - Added experimental FTP service for networking modules 'vnet' and 'socket'
  - Fixes and improvements for all supported Voodoo graphics adapters
  - Added USB keyboard emulation with most of the keys supported
  - GUI "cmdmode": create a headerbar event with key combo starting with F7
  - LGPL'd VGABIOS updated to version 0.8a (new VGABIOS for Voodoo Banshee)

See CHANGES file for more information!
